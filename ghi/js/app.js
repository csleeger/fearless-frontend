function createCard(name, description, pictureUrl) {
    return `
    <div class="col-md-4">
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <p class="card-text">${description}</p>
            </div>
        </div>
    <div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            //Figure out what to do when bad response
        } else {
            const data = await response.json();

            const row = document.querySelector('#card-container');

            for (let conference of data.conferences) {
                const detailURL = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailURL);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const html = createCard(name, description, pictureUrl);
                    row.innerHTML += html;
            }
            }
        }
    } catch (e) {
        console.error(e);
        // Figure out what to do on error
    }

});
